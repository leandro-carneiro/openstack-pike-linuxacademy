# -*- mode: ruby -*-
# vi: set ft=ruby :

LIBVIRT_HOST = ENV['LIBVIRT_HOST'] || "10.10.10.195"
BOX = "generic/ubuntu1604"

required_plugins = %w(vagrant-hostsupdater vagrant-puppet-install vagrant-libvirt)
required_plugins.each do |plugin|
  system "vagrant plugin install #{plugin}" unless Vagrant.has_plugin? plugin
end

Vagrant.configure("2") do |config|
  machines=[
    { 
      :hostname => "controller",
      :network  => {
        :ipPriv => "192.168.122.11", 
        :ipPub  => "172.16.113.11",
      },
      :ram      => 4096,
      :cpu      => 1,
      :disks    => nil,
    },
    { 
      :hostname => "compute",
      :network  => {
        :ipPriv => "192.168.122.31", 
        :ipPub  => "172.16.113.31",
      },
      :ram      => 4096,
      :cpu      => 1,
      :disks    => nil,
    },
    { 
      :hostname => "block1",
      :network  => {
        :ipPriv => "192.168.122.41",
        :ipPub  => nil,
      },
      :ram      => 2048,
      :cpu      => 1,
      :disks    => [10,],
    },
    { 
      :hostname => "object1",
      :network  => {
        :ipPriv => "192.168.122.51",
        :ipPub  => nil,
      },
      :ram      => 2048,
      :cpu      => 1,
      :disks    => [8,8,],
    },
    { 
      :hostname => "object2",
      :network  => {
        :ipPriv => "192.168.122.52",
        :ipPub  => nil,
      },
      :ram      => 2048,
      :cpu      => 1,
      :disks    => [8,8,],
    },
  ]

    machines.each do |machine|
      config.vm.define machine[:hostname] do |node|
          node.vm.network :private_network, :ip => machine[:network][:ipPriv], :mode => "nat"
          unless machine[:network][:ipPub].nil?
            node.vm.network :private_network, :dev => :virbr2, :mode => "nat", :ip => machine[:network][:ipPub]
          end
          node.vm.box = BOX
          node.hostsupdater.aliases = [ machine[:hostname] ]
          node.vm.hostname = machine[:hostname] + ".libvirt"
          node.vm.provider :libvirt do |libvirt|
             libvirt.host = LIBVIRT_HOST
             libvirt.username = "root"
             libvirt.id_ssh_key_file = "/opt/id_rsa_libvirt"
             libvirt.connect_via_ssh = true
             libvirt.storage_pool_name = "HDs"
             libvirt.graphics_type = "vnc"
             libvirt.keymap = "en-us"
             libvirt.suspend_mode = "managedsave"
             libvirt.features = [ 'acpi', 'apic', 'pae' ]
             libvirt.graphics_ip = '0.0.0.0'
             libvirt.cpu_mode = 'host-passthrough' #Para Ativar VMX: echo "options kvm-intel nested=1" | sudo tee /etc/modprobe.d/kvm-intel.conf ;reboot (no host)
             libvirt.cpus = machine[:cpu]
             libvirt.memory = machine[:ram]
             unless machine[:disks].nil?
                machine[:disks].each do |disk|
                  libvirt.storage :file, :size => disk, :type => 'qcow2'
                end
             end
          end
      end
    end

  config.vm.provision "shell", inline: <<-END_OF_SHELL
    echo -e "192.168.122.11\tcontroller\n172.16.113.11\tcontroller-api\n192.168.122.31\tcompute\n172.16.113.31\tcompute-api\n192.168.122.41\tblock1\n192.168.122.51\tobject1\n192.168.122.52\tobject2" >> /etc/hosts

    apt update
    apt install chrony software-properties-common -y
    add-apt-repository cloud-archive:pike -y
    add-apt-repository cloud-archive:pike-updates -y
    apt update && apt dist-upgrade -y
    apt install python-openstackclient -y

    if [ "$HOSTNAME" == "controller" ]; then
      echo "allow 192.168.122.0/24" >> /etc/chrony/chrony.conf
      apt install mariadb-server python-pymysql rabbitmq-server  memcached python-memcache -y
      rabbitmqctl add_user openstack OpenStack2018
      rabbitmqctl set_permissions openstack ".* " ".*" ".*"
      sed -e 's/-l 127.0.0.1/-l 192.168.122.11/g' -i  /etc/memcached.conf
      service memcached restart
      echo -e "[mysqld]\nbind-address = 192.168.122.11\ndefault-storage-engine = innodb\ninnodb_file_per_table = on\nmax_connections = 4096\ncollation-server = utf8_general_ci\ncharacter-set-server = utf8" > /etc/mysql/mariadb.conf.d/99-openstack.conf
      service mysql restart
      mysql_secure_installation <<EOF

y
OpenStack2018
OpenStack2018
y
y
y
y
EOF
    else
      sed -e 's/^pool.*/#&/g' -i /etc/chrony/chrony.conf
      echo "server controller iburst" >> /etc/chrony/chrony.conf
    fi

    service chrony restart
    chronyc sources
  END_OF_SHELL

end

